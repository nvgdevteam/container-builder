#!/usr/bin/env python
from __future__ import print_function
import sys
import json
import os
import io
import ConfigParser
import osaka.main
import hysds_commons.request_utils
from hysds.celery import app
if __name__ == "__main__":
    '''
    Main program routing arguments to file
    '''
    if len(sys.argv) != 6:
        print("[ERROR] Metadata dataset.json generation requires a version, and archive file",sys.stderr)
        sys.exit(-1)
    #Read arguments
    ident=sys.argv[1]
    version=sys.argv[2]
    product=sys.argv[3]
    repo=sys.argv[4]
    digest=sys.argv[5]
    url = os.path.join(repo,os.path.basename(product))
    #OSAKA call goes here

    #osaka.main.put("./"+product,url,params={"encrypt":{"type":"AES256"}})
    with open(os.path.expanduser(os.path.join('~', '.azure', 'config'))) as f:
        sample_config = f.read()
    config = ConfigParser.RawConfigParser(allow_no_value=True)
    config.readfp(io.BytesIO(sample_config))
    osaka.main.put("./"+product,url,params={"encrypt":{"type":"AES256"}, "account_name": config.get('storage', 'account'), "account_key": config.get('storage', 'key')})

    metadata = {"name":ident,"version":version,"url":url,"resource":"container", "digest":digest}
    hysds_commons.request_utils.requests_json_response("POST", os.path.join(app.conf["MOZART_REST_URL"],"container/add"), data=metadata, verify=False)
    sys.exit(0)
